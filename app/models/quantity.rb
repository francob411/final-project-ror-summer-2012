class Quantity < ActiveRecord::Base
  attr_accessible :amount, :recipe_ingredients_attributes

  has_many :recipe_ingredients

  before_save :normalize

  private

  def normalize
    self.amount = self.amount.strip.downcase if self.amount
  end
 

end
