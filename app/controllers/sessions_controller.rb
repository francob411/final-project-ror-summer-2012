class SessionsController < ApplicationController
  skip_before_filter :login_required, :only => [ :new, :create ]
  

  def new
  	render login_path
  end

  def create
  	user = User.find_by_email(params[:email])

  	if user && user.authenticate(params[:password])
  	  session[:user_id] = user.id
      redirect_to recipes_path, notice: "#{user.email}, You have successfully logged in." 
    else
     redirect_to login_path, {:notice => "Login failed for some reason! dude..."}

    end
  end

  def destroy
    logout
  	redirect_to login_path, :notice => 'You have successfully logged out. Come back soon!' 
  end

 private



end


	