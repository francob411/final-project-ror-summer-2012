# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create(email: "avi@email.com", first_name: "avi", last_name: "flombaum", password: "password")
User.create(email: "franco@email.com", first_name: "franco", last_name: "barbeite", password: "password")
