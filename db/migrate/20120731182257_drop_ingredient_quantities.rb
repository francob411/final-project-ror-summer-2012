class DropIngredientQuantities < ActiveRecord::Migration
  def change
    drop_table :ingredient_quantities 
  end

end
