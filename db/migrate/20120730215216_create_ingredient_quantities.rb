class CreateIngredientQuantities < ActiveRecord::Migration
  def change
    create_table :ingredient_quantities do |t|
      t.integer :ingredient_id
      t.integer :quantity_id

      t.timestamps
    end
  end
end
